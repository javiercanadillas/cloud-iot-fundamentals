
#!/usr/bin/env bash

PROJECT_ID=javiercm-main-demos
REGION=europe-west1
TOPIC_NAME=my-topic
SUB_NAME=my-sub
REGISTRY_NAME=tour-registry
DEVICE_NAME=test-dev

# Create a pubsub topic
gcloud pubsub topics create tour-pub --project="${PROJECT_ID}"
# Create a subscription to the newly created topic
gcloud pubsub subscriptions create "${SUB_NAME}" --topic="${TOPIC_NAME}"

# Create a Cloud IoT Device Registry
# --event-notification-config=[topic=TOPIC] marks the configuration for
# notification of telemetry events received from the device.
gcloud iot registries create "${REGISTRY_NAME}" --region="${REGION}" \
    --event-notification-config=topic="${TOPIC_NAME}"

# Generate RSA public and private keys that will be used for authenticating
# your virtual device when it connects. Do not transmit the private key anywhere
# other than the device and only use it to generate an authorization credential
openssl req -x509 -newkey rsa:2048 -days 3650 -keyout rsa_private.pem \
    -nodes -out rsa_public.pem -subj "/CN=unused"

# Register the device using the public key
gcloud iot devices create "${DEVICE_NAME}" --region="${REGION}" \
  --registry="${REGISTRY_NAME}" \
  --public-key path=rsa_public.pem,type=rs256

