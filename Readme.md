# Instructions

This tutorial is based on the Google Cloud Platform code that you can find [here](https://github.com/GoogleCloudPlatform/python-docs-samples). 

# Foundations

## Understanding PubSub

We'll be using Google Cloud Pub/Sub as a way to store and retrieve messages:

![PubSub overview](https://cloud.google.com/pubsub/images/pub_sub_flow.svg)

First, we need to create a Pub/Sub topic. We do that consuming the Pub/Sub API in one of the three different ways:
- Graphically, using the Google Cloud Console
- Via CLI, using the `gcloud` tool that is part of the Google Cloud SDK that you can download in your machine.
- Programmatically consuming the API, either via REST/gRPC or using the Google Cloud Client Libraries for the language you've chosen.

Here, we're going to explore two ways of doing it

### Creating a Pub/Sub topic and subscription via CLI

To create a Pub/Sub topic and subscription via the `gcloud` command, we'll use the `pubsub` subcommand in a very easy way:

```bash
gcloud pubsub topics create my-topic
```

Now, we can subscribe or publish to it. Create a subscription so we can write to it:

```bash
gcloud pubsub subscriptions create my-sub --topic my-topic
```

### Creating a Pub/Sub topic and subscription via REST API

To emulate this, we'll use the very convenient API explorer. In real life, we would be doing this through code, but when we're starting to get ourselves used to a new API, it's always a good idea to start fiddling around with the API Explorer first.

So let's use it to call the `projects.topics.create method`. [Go to the Reference Documentation page for the method](https://cloud.google.com/pubsub/docs/reference/rest/v1/projects.topics/create) and you will see a "Try this API" panel on the right of the screen.

Now fill out the method so that the name field matches the following:
```text
projects/<your-project-id>/topics/my-topic
```

(Remember to substitute <your-project-id> by your Google Cloud project ID)

Make sure that there are no trailing spaces in the name field. Click the Execute button, choose your Google Account in the project and click Allow.

Your response should resemble the following:
```text
200
- Show headers -
{
"name": "projects/<your-project-id>/topics/my-topic"
}
```

The devices in this system publish temperature data to their telemetry feeds, and the server will consume telemetry data from the Cloud Pub/Sub topic you just created.

Now to create the subscription, we'll follow the same process. [Go to the Reference Documentation page for the method `projects.subscription.create`](https://cloud.google.com/pubsub/docs/reference/rest/v1/projects.subscriptions/create) and you will see a "Try this API" panel on the right of the screen.

Now fill out the method so that the name field matches the following:
```text
projects/<your-project-id>/topics/my-topic
```

(Remember to substitute <your-project-id> by your Google Cloud project ID)

Make sure that there are no trailing spaces in the name field. Click the Execute button, choose your Google Account in the project and click Allow.

Your response should resemble the following:
```text
200
- Show headers -
{
  "name": "projects/<your-project-id>/subscriptions/my-sub",
  "topic": "projects/<your-project-id>/topics/my-topic",
  "pushConfig": {},
  "ackDeadlineSeconds": 10,
  "messageRetentionDuration": "604800s",
  "expirationPolicy": {
    "ttl": "2678400s"
  }
}
```

### Publishing messages to our new topic
You can now publish a message in my-topic using Python code. This will be pretty convenient as Python and Micropython are both syntax compatible:

```bash
python publisher.py
```

### Getting the messages from the topic, asynchronously

![Pulling messages](https://cloud.google.com/pubsub/images/subscriber_pull.svg)

```bash
subscriber.py
```

# Understanding MQTT

TBC

# Understanding IoT Core

TBC

# Running the end to end example

## Creating an IoT Registry

TBC

## Generating RSA keys and creating a device

TBC

## Check the Device-Server flow

TBC

### Start the virtual server

TBC

### Start the virtual device and observe

TBC

# Additional documentation 

- [Paho MQTT Client](https://pypi.org/project/paho-mqtt/#client)
- [Cloud IoT Core - Publishing over the MQTT Bridge](https://cloud.google.com/iot/docs/how-tos/mqtt-bridge)
- [Cloud IoT Core - Device Security](https://cloud.google.com/iot/docs/concepts/device-security)